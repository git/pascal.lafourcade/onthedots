<html>
<head>
<title>On The Dot</title>
<link rel="stylesheet" type="text/css" href="index.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/monolith.min.css"/> <!-- 'monolith' theme -->
<script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
</head>
<body>
<script>console.log("index");</script>
<div>
    <div class="logo">
        <img id="btn_options" src="./ress/gear.png" class="settingsButton" />
        <img src="ress/logo_dark.png" id="logo"/>
        <img src="ress/light" id="light" class="lightButton" onClick="switchLight()"/>
        
    </div>
    
</div>


<form action="index.php" id="high_form" method="post">
        <div class="mode-container">
            <ul id="diff" class="all-container">
                <input type="radio" class="rad" name="difficulty" id="easy" value="easy" />
                <label for="easy"><img id="img_easy" class="button" src="./ress/button_diff_easy.png"/></label>
                    
                <input type="radio" class="rad" name="difficulty" id="med" value="med" />
                <label for="med"><img id="img_med" class="button" src="./ress/button_diff_normal.png"/></label>
                    
                <input type="radio" class="rad" name="difficulty" id="hard" value="hard" />
                <label for="hard"><img id="img_hard" class="button" src="./ress/button_diff_hard.png"/></label>
            </ul>

            <div id="modeSelection" class="mode-container">
                <ul id="modes" class="all-container">
                    <img id="img_points" class="button" src="./ress/button_mode_points.png"/>

                    <img id="img_temps" class="button" src="./ress/button_mode_temps.png"/>

                    <img id="img_survie" class="button" src="./ress/button_mode_survie.png"/>
                </ul>
                <ul id="points" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="5p" value="5p" />
                    <label for="5p"><img id="img_5p" class="button" src="./ress/button_points_5.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="10p" value="10p"/>
                    <label for="10p"><img id="img_10p" class="button mid" src="./ress/button_points_10.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="15p" value="15p" />
                    <label for="15p"><img id="img_15p" class="button" src="./ress/button_points_15.png"/></label>

                    <!--<input type="radio" class="rad" name="game_mode" id="20p" value="20p" />
                    <label for="20p"><img id="img_20p" class="button" src="./ress/button_points_20.png"/></label>-->

                </ul>
                <ul id="temps" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="2m" value="2m" />
                    <label for="2m"><img id="img_2m" class="button" src="./ress/button_temps_2.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="3m" value="3m" />
                    <label for="3m"><img id="img_3m" class="button mid" src="./ress/button_temps_3.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="5m" value="5m" />
                    <label for="5m"><img id="img_5m" class="button" src="./ress/button_temps_5.png"/></label>

                    <!--<input type="radio" class="rad" name="game_mode" id="10m" value="10m"/>
                    <label for="10m"><img id="img_10m" class="button" src="./ress/button_temps_10.png"/></label>-->

                </ul>
                <ul id="survie" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="15s" value="15s"/>
                    <label for="15s"><img id="img_15s" class="button" src="./ress/button_survie_15.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="30s" value="30s" />
                    <label for="30s"><img id="img_30s" class="button mid" src="./ress/button_survie_30.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="45s" value="45s" />
                    <label for="45s"><img id="img_45s" class="button" src="./ress/button_survie_45.png"/> </label>

                </ul>

            </div>
        
            <div>
                <ul id="pause" class="all-container ad" style="display :flex;">
                    <input type="radio" class="rad" name="pause" id="pause_1" value="1" />
                    <label for="1"><img id="img_pause_1" class="button" src="./ress/button_pause_1.png"/></label>

                    <input type="radio" class="rad" name="pause" id="pause_0" value="0"/>
                    <label for="0"><img id="img_pause_0" class="button" src="./ress/button_pause_0.png"/></label>


                </ul>

                <div class="slot">   
                    <p class="texte">
                    <img src="ress/-" onClick="less()" id="-"/>
                    <span class="slot" id="dotsDisplayer">
                        <span id="col1D" class="dot"></span>
                        <span id="col2D" class="dot"></span>
                        <span id="col3D" class="dot"></span>
                        <span id="col4D" class="dot"></span>
                        <span id="col5D" class="dot"></span>
                        <span id="col6D" class="dot"></span>
                        <span id="col7D" class="dot"></span>
                        <span id="col8D" class="dot"></span>

                        <img id="img_gem1" class="gem"/>
                        <img id="img_gem2" class="gem"/>
                        <img id="img_gem3" class="gem"/>
                        <img id="img_gem4" class="gem"/>
                        <img id="img_gem5" class="gem"/>
                        <img id="img_gem6" class="gem"/>
                        <img id="img_gem7" class="gem"/>
                        <img id="img_gem8" class="gem"/>

                        <span id="gem1D" class="slot"></span>
                        <span id="gem2D" class="slot"></span>
                        <span id="gem3D" class="slot"></span>
                        <span id="gem4D" class="slot"></span>
                        <span id="gem5D" class="slot"></span>
                        <span id="gem6D" class="slot"></span>
                        <span id="gem7D" class="slot"></span>
                        <span id="gem8D" class="slot"></span>
                        <!--
                        <img id="gem1D" class=" slot" />
                        <img id="gem2D" class=" slot" />
                        <img id="gem3D" class=" slot" />
                        <img id="gem4D" class=" slot" />
                        <img id="gem5D" class=" slot" />
                        <img id="gem6D" class=" slot" />
                        <img id="gem7D" class=" slot" />
                        <img id="gem8D" class=" slot" />-->
                    </span>
                    <img src="ress/+" onClick="more()" id="+"/>
                    <input type="hidden" name="dotsAmount" id="amountHi"/></p>
                </div>
            </div>
        </div>
    </form>
    <?php

$database = new SQLite3("data.db");

// echo isset($_POST["game_mode"])." ".isset($_POST["difficulty"])." ".isset($_POST["dotsAmount"])." ".isset($_POST["pause"]);
$toDisp=5;
if(isset($_POST["game_mode"]) && $_POST["game_mode"]!="" && $_POST["difficulty"]!="" && $_POST["dotsAmount"]!="" && $_POST["pause"]!="")
{
    $db_game_mode=$_POST["game_mode"];
    $db_difficulty=$_POST["difficulty"];
    $db_dotsAmount=$_POST["dotsAmount"];
    $db_pause=$_POST["pause"];
}
else
{
    $db_game_mode="5p";
    $db_difficulty="easy";
    $db_dotsAmount=(isset($_COOKIE["dotsAmount"])) ? $_COOKIE["dotsAmount"] : 4;
    $db_pause=(isset($_COOKIE["pause"])) ? $_COOKIE["pause"] : 1;
}

if(stristr($db_game_mode, "p") === FALSE)
{
    $sql='select score, player from score where game_mode="'.$db_game_mode.'" and difficulty="'.$db_difficulty.'" and dots_amount='.$db_dotsAmount.' and pause='.$db_pause.' order by score desc limit '.$toDisp;
    $unit="points";
}
else
{
    $sql='select score, player from score where game_mode="'.$db_game_mode.'" and difficulty="'.$db_difficulty.'" and dots_amount='.$db_dotsAmount.' and pause='.$db_pause.' order by score asc limit '.$toDisp;
    $unit="secondes";
}
$result = $database->query($sql);
$a=$result->fetchArray();

if($a==false)
{
    $phrase= "Il n'y a pas encore de score pour ce mode de jeu.";
}
else
{
    $phrase="1° ".$a["player"].", ".$a[0]." ".$unit;
    $cpt=2;
    while ($row = $result->fetchArray()) {

        $phrase=$phrase."</br>".$cpt."° ".$row["player"].", ".$row["score"]." ".$unit;
        $cpt++;
    }

}

echo "<h1 id='aya' class='texte mode-container' >".$phrase."</h1>";

if(isset($_POST["game_mode"]) && $_POST["game_mode"]!="" && $_POST["difficulty"]!="" && $_POST["dotsAmount"]!="" && $_POST["pause"]!="")
{
    echo "<input type='hidden' id='modeSel' value='".$_POST["game_mode"]."'/>";
    echo "<input type='hidden' id='diffSel' value='".$_POST["difficulty"]."'/>";
    echo "<input type='hidden' id='dotsAmountSel' value='".$_POST["dotsAmount"]."'/>";
    echo "<input type='hidden' id='pauseSel' value='".$_POST["pause"]."'/>";
}

?>
<div class="mode-container">
    <ul id="btns_play">
        <img id="btn_play" src="ress/button_play.png" onClick="play()"/>
    </ul>
</div>

<img id="btn_patchnote" src="./ress/button_patchnote.png" class="bg"/>

<div id="patchnote" class="modal">
    <div class="modal-content" id="patchnoteContenu">
    <span class="close" id="close_patchnote">&times;</span>
    <p class="texte">Notes de mise à jour</p>
    <!--<iframe src="patchnote1.txt" class="texte patchnote" id="patchnote_content"></iframe>-->
    <?php
        $ct=htmlspecialchars(file_get_contents("patchnote1.txt"));
        $ct=str_replace("\n","<br/>",$ct);
        //var_dump($ct);
        echo '<p class="texte">'.$ct.'</p>';
    ?>
    </div>
</div>

<!-- The Modal -->
<div id="optionsModal" class="modal">

<!-- Modal content -->
<div class="modal-content" id="modal">
    <span class="close" id="close_options">&times;</span>
    <p class="texte ad">Options</p>
    <div class="slidecontainer">        
            <div id="displays">
            <p class="texte">
            Mode d'affichage 
            <span id="points_di" class="dot" onClick="selectDisplay('points')"></span>
            <img id="gems_di" src="ress/green.png" onClick="selectDisplay('gems')"/>
            <span id="both_di" class="dot both" onClick="selectDisplay('both')"></span>
        </p>
        </div>
                
        <div id="color_pickers" class="color-pickers">
            <div class="texte"> Couleurs des dots  
                <span id="col1" class="dot col1 pickr"></span>
                <span id="col2" class="dot col2 pickr"></span>
                <span id="col3" class="dot col3 pickr"></span>
                <span id="col4" class="dot col4 pickr"></span>
                <span id="col5" class="dot col5 pickr"></span>
                <span id="col6" class="dot col6 pickr"></span>
                <span id="col7" class="dot col7 pickr"></span>
                <span id="col8" class="dot col8 pickr"></span>
            </div>
        </div>
        <div id="gems_picker">
            <img id="gem1" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem2" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem3" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem4" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem5" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem6" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem7" class="gem" draggable="true" ondragstart="drag(event)"/>
            <img id="gem8" class="gem" draggable="true" ondragstart="drag(event)"/>
            <div>
                <p class="texte">Gemmes à afficher</p>
            <span id="slot1" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot2" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot3" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot4" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot5" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot6" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot7" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <span id="slot8" class="slot" ondrop="drop(event)" ondragover="allowDrop(event)"></span>
            <img id="resetGems2" onClick="resetGems()" src="ress/reset"/>
            </div>
        </div>
        <div class=" mode-container z">
            <img src="ress/OK.png" id="btn_confirm" onClick="saveOptions()"/>
        </div>
    </div>
</div>

</div>
<script src="bakery.js"></script>
<script src="button_displayer.js"></script>
<script src="options.js"></script>
<script src="patchnote.js"></script>
<script src="modals.js"></script>
<script src="themeSwitcher.js"></script>
<script>
    dispDots();
</script>
</body>
</html>
