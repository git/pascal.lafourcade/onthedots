function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

var get=$_GET();

if(document.getElementById("canvasR")!=null)
{
    document.getElementById("canvasR").style.display="none";
    document.getElementById("reserveText").style.display="none";
}
if(document.getElementById("canvasTotal")!=null)
{
	if (get['mode'].includes('p'))
	{
		objPts=get['mode'].substring(0,get['mode'].length-1);
		document.getElementById("timer").style.display="none";
	}
	else if(get['mode'].includes('m'))
	{
		objPts=null;
		tpsRem=[0,parseInt(get['mode'].substring(0,get['mode'].length-1),10),0];
		document.getElementById("chronotime").style.display="none";
	}
	else if(get['mode'].includes('s'))
	{
		objPts=null;
		tpsRem=[0,1,0];
		tpsPlus=[0,parseInt(get['mode'].substring(0,get['mode'].length-1),10),0];
		document.getElementById("chronotime").style.display="none";
	}
}


difficulty=get["diff"];
console.log("difficulté = "+difficulty+" mode:"+get['mode']);