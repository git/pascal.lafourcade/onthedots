<html>
<head>

<link rel="stylesheet" type="text/css" href="index.css"/>
</head>
<body>
    <div class="logo">
        <img src="ress/logo_dark.png" id="logo"/>
        <img src="ress/light" id="light" class="lightButton" onClick="switchLight()"/>
        
    </div>

<script src="selecteur.js"></script>
<p class="texteG" id="mode">Mode de jeu : </p>
<p class="texteG" id="points"></p>

<?php
$database = new SQLite3("data.db");
$toDisp=5;
if(stristr($_GET['mode'], "p") === FALSE)
{
    $sql='select score, player from score where game_mode="'.$_GET['mode'].'" and difficulty="'.$_GET['diff'].'" and dots_amount='.$_COOKIE["dotsAmount"].' and pause='.$_COOKIE["pause"].' order by score desc limit '.$toDisp;
    $unit="points";
}
else
{
    $sql='select score, player from score where game_mode="'.$_GET['mode'].'" and difficulty="'.$_GET['diff'].'" and dots_amount='.$_COOKIE["dotsAmount"].' and pause='.$_COOKIE["pause"].' order by score asc limit '.$toDisp;
    $unit="secondes";
}

$result = $database->query($sql);
$a=$result->fetchArray();

if($a==false)
{
    echo "<p class='texteG'>Vous avez le premier score de ce mode de jeu.</p>";
}
else
{
    $phrase="1° ".$a["player"].", ".$a[0]." ".$unit;
    $cpt=2;
    while ($row = $result->fetchArray()) {

        $phrase=$phrase."</br>".$cpt."° ".$row["player"].", ".$row["score"]." ".$unit;
        //$phrase+=;
        $cpt++;
    }
        
    echo "<h1 id='aya' class='texteG mode-container' >".$phrase."</h1>";
}
?>
</br>
<?php 
if($_GET['pseudo']!="")
{
    if(stristr($_GET['mode'], "p") === FALSE) // vérification du mode pour afficher le score le plus grand ou le plus petit (- de temps = + de score)
    { //il n'y a pas "p" dans le mode, donc on cherche le plus haut score
        $sqlScore='select max(score) from score where game_mode="'.$_GET['mode'].'" and difficulty="'.$_GET['diff'].'" and dots_amount='.$_COOKIE["dotsAmount"].' and pause='.$_COOKIE["pause"].' and player="'.$_GET['pseudo'].'"';
    }
    else
    { // on est en points, donc on cherche le temps le plus faible
        $sqlScore='select min(score) from score where game_mode="'.$_GET['mode'].'" and difficulty="'.$_GET['diff'].'" and dots_amount='.$_COOKIE["dotsAmount"].' and pause='.$_COOKIE["pause"].' and player="'.$_GET['pseudo'].'"';
    }
    $result = $database->query($sqlScore); // on prend le meilleur score du joueur
    $data = $result->fetchArray()[0];

    if($data==null) // Le joueur n'a pas encore de score enregistré
    {
        $sql='select count(*) from game where game_mode="'.$_GET['mode'].'" and difficulty="'.$_GET['diff'].'" and dots_amount='.$_COOKIE["dotsAmount"].' and pause='.$_COOKIE["pause"];
        $result = $database->query($sql);

        if($result->fetchArray()[0]==0) // Si le mode de jeu 'nest pas déjà dans la base, on l'ajoute
        {
            $sql='insert into game values("'.$_GET['diff'].'","'.$_GET['mode'].'","---",'.$_COOKIE["dotsAmount"].','.$_COOKIE["pause"].')';
        }

        $sql='insert into score values("'.$_GET['pseudo'].'","'.$_GET['diff'].'","'.$_GET['mode'].'",'.$_COOKIE["dotsAmount"].','.$_COOKIE["pause"].',date("now"),'.$_GET['pts'].')';
        $database->query($sql); // enregistrement du score du joueur
    }
    else // Le joueur a un score enregistré, vérification s'il dépasse son record
    {
        $result = $database->query($sqlScore)->fetchArray()[0];
        //Si le joueur a un meilleur score (selon le critère du mode), mise à jour
        if(($data<$_GET['pts'] && stristr($_GET['mode'], "p") === FALSE) || ($data>$_GET['pts'] && stristr($_GET['mode'], "p") !== FALSE))
        {
            $sql="update score set score=".$_GET['pts'].",date=date('now') where player='".$_GET['pseudo']."' and game_mode='".$_GET['mode']."' and difficulty='".$_GET['diff']."' and dots_amount=".$_COOKIE["dotsAmount"]." and pause=".$_COOKIE["pause"];
            $database->query($sql);

            $result=$data;
            
        }
        echo "<input type='hidden' id='rec' value='".$result."'/>";
    }
}
?>
<div class="mode-container">
    <a href="./" rel="external"><img id="btn_index" src="ress/button_index.png" class="back" /></a>
    <img id="btn_replay" src="ress/button_replay.png" class="back" onClick="play()"/>
</div>
<script>
size=screen.height/6;
phrase=get['pseudo']+", votre "+(get['mode'].includes('p') ? "temps" : "score") +" est "+ get['pts'] +" "+(get['mode'].includes('p') ? "secondes" : "points");
if(document.getElementById("rec")!=null)
{
    phrase+= (document.getElementById("rec").value==get['pts'] ? " et c'est votre record" : " et votre record est "+ document.getElementById("rec").value +" "+(get['mode'].includes('p') ? "secondes" : "points"));
}
document.getElementById("points").innerHTML = phrase;
document.getElementById("btn_index").width=size;
document.getElementById("btn_replay").width=size;

function play()
{
    var pseudo = prompt("Saisissez votre pseudo", getCookie("pseudo"));
    //setCookie("pause",(document.getElementById("pause_1").checked ? "1" : "0"),30);
    if(pseudo!="")
    {
        setCookie("pseudo",pseudo,30);
    }
    
    window.location.replace("./game.html?mode="+get['mode']+"&diff="+get['diff']+"&pseudo="+ (pseudo==null ? "" : pseudo));
}

</script>

<script src="bakery.js"></script>
<script src="themeSwitcher.js"></script>
<script src="infoDisplayer.js"></script>
</body>
</html>
