// When the user clicks anywhere outside of the modalPatch, close it
window.onclick = function(event) {
    if (event.target == modalPatch || event.target == modalOptions) {
        console.log(modalPatch.style.display + " " + modalOptions.style.display);
        modalPatch.style.display = "none";
        modalOptions.style.display = "none";
        console.log(modalPatch.style.display + " " + modalOptions.style.display);

    }
}

// When the user clicks on <spanPatch> (x), close the modalPatch
spanPatch.onclick = function() {
    modalPatch.style.display = "none";
}

// When the user clicks on <spanOptions> (x), close the modalOptions
spanOptions.onclick = function() {
    modalOptions.style.display = "none";
}
