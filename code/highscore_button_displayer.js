var modes=["points","temps","survie"];
var diffs=["easy","med","hard"];
var diffSelected="";
var modeSelected="";
size=screen.height/6;
hideAll();

document.getElementById("btn_play").width=size;

modes.forEach(element => {

    eval("var img_"+element+"= document.getElementById('img_"+element+"')");
    eval("img_"+element+".width=size");
    eval("img_"+element).addEventListener("mousedown",function(e)
    {
        disp(e);
        modes.forEach(element => {
            document.getElementById("img_"+element).classList.remove("selected");
        })
        document.getElementById(e.currentTarget.id).classList.add("selected");
    });

    var tab=document.getElementById(element).children;
    for(var i=1;i<tab.length;i+=2){
        tab.item(i).children[0].width=size;
        tab.item(i).children[0].addEventListener("mousedown",function(e)
        {
            var tab=document.getElementById("modeSelection").children;
            for(var cpt=1;cpt<tab.length;cpt++)
            {
                var tabChilds=document.getElementById(tab.item(cpt).id).children;
                for(var cptChild=1;cptChild<tabChilds.length;cptChild+=2)
                {
                    tabChilds.item(cptChild).children[0].classList.remove("selected");
                }
            }
            
            modeSelected= e.currentTarget.id.substring(4,e.currentTarget.id.length);

            document.getElementById(e.currentTarget.id).classList.add("selected");
            if(diffSelected!="" && modeSelected!="")
        {
            document.getElementById(modeSelected).checked=true;

            document.getElementById("high_form").submit();
        }
        });
    }
});

diffs.forEach(element => {
    eval("var img_"+element+"= document.getElementById('img_"+element+"')");
    eval("img_"+element+".width=size");
    eval("img_"+element).addEventListener("mousedown",function(e)
    {
        diffSelected= e.currentTarget.id.substring(4);
        var tab=document.getElementById("diff").children;
        for(var cpt=1;cpt<tab.length;cpt+=2)
        {
            tab.item(cpt).children[0].classList.remove("selected");
        }
        document.getElementById(e.currentTarget.id).classList.add("selected");
        if(diffSelected!="" && modeSelected!="")
        {
            document.getElementById(diffSelected).checked=true;
            document.getElementById("high_form").submit();
        }
    });
})

function disp(what,short=false)
{
    what=(short ? what : what.currentTarget.id.substring(4));
    console.log(what);
    modes.forEach(element => {
        document.getElementById(element).style.display=what==element ? "flex":"none";
    })
}

function hideAll()
{
    document.getElementById("temps").style.display="none";
    document.getElementById("points").style.display="none";
    document.getElementById("survie").style.display="none";
}

// Dots amount

var output = document.getElementById("dotsNumber");
var outputHi = document.getElementById("amountHi");
var amount = (getCookie("dotsAmount")=="" ? 4 : getCookie("dotsAmount"));
output.innerHTML = amount;
outputHi.value= amount;

document.getElementById("+").width=size/4;
document.getElementById("-").width=size/4;

function less()
{
  if(amount > 2)
  {
    amount--;
  }
  output.innerHTML = amount;
  outputHi.value=amount;
}

function more()
{
  if(amount<8)
  {
    amount++;
  }
  output.innerHTML = amount;
  outputHi.value=amount;
}

// pause oui/non

document.getElementById("pause"+ (getCookie("pause")!="" ? getCookie("pause") : 0)).checked=true;

// play button

function play()
{
    setCookie("dotsAmount",amount,30);
    setCookie("pause",(document.getElementById("pause0").checked ? 0 : 1),30);
    var pseudo = prompt("Saisissez votre pseudo", '');
    window.location.replace("./game.html?mode="+modeSelected+"&diff="+diffSelected+"&pseudo="+ (pseudo==null ? "" : pseudo));
}

function index()
{
    window.location.replace("./");
}

if(document.getElementById("diffSel")!=null)
{
    console.log("LOADING");
    diffSelected=document.getElementById("diffSel").value;
    modeSelected=document.getElementById("modeSel").value;
    amount=document.getElementById("dotsAmountSel").value;
    gameSelected = (modeSelected.includes("s") ? "survie" : (modeSelected.includes("p") ? "points" : "temps"));

    console.log("LOADED "+diffSelected+" " + modeSelected+ " "+ amount + " ");

    disp(gameSelected, true);
    document.getElementById("img_"+diffSelected).classList.add("selected");
    document.getElementById("img_"+gameSelected).classList.add("selected");
    document.getElementById("img_"+modeSelected).classList.add("selected");
    document.getElementById("pause"+ document.getElementById("pauseSel").value).checked=true;

    document.getElementById(diffSelected).checked=true;
    document.getElementById(modeSelected).checked=true;

    output.innerHTML = amount;
    outputHi.value= amount;
}
else
{
    diffSelected="easy";
    modeSelected="5p";
    disp("points",true);
    document.getElementById(diffSelected).checked=true;
    document.getElementById(modeSelected).checked=true;
    document.getElementById("img_points").classList.add("selected");
    document.getElementById("img_5p").classList.add("selected");
    document.getElementById("img_easy").classList.add("selected");
}

