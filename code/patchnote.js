// Get the modalPatch
var modalPatch = document.getElementById("patchnote");

// Get the button that opens the modalPatch
var btnPatch = document.getElementById("btn_patchnote");

// Get the <spanPatch> element that closes the modalPatch
var spanPatch = document.getElementById("close_patchnote");

// When the user clicks the button, open the modalPatch 
btnPatch.onclick = function() {
    modalPatch.style.display = "block";
}

// When the user clicks on <spanPatch> (x), close the modalPatch
spanPatch.onclick = function() {
    modalPatch.style.display = "none";
}
/*
// When the user clicks anywhere outside of the modalPatch, close it
window.onclick = function(event) {
    if (event.target == modalPatch) {
        modalPatch.style.display = "none";
        modalOptions.style.display = "none";

    }
}

// When the user clicks on <spanPatch> (x), close the modalPatch
spanPatch.onclick = function() {
    modalPatch.style.display = "none";
}
*/

var content = document.getElementById("patchnote_content");
//content.style.width=modalPatch.width;