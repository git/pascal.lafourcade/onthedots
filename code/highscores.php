<head>
<link rel="stylesheet" type="text/css" href="highscores.css"/>
</head>
<body>
<div>
    <div class="logo">
        <img src="ress/logo_dark.png" id="logo"/>
        <img src="ress/light" id="light" class="lightButton" onClick="switchLight()"/>
        
    </div>
    
</div>

    <form action="highscores.php" id="high_form" method="post">
        <div class="mode-container">
            <ul id="diff" class="all-container">
                <input type="radio" class="rad" name="difficulty" id="easy" value="easy" />
                <label for="easy"><img id="img_easy" class="button" src="./ress/button_diff_easy.png"/></label>
                    
                <input type="radio" class="rad" name="difficulty" id="med" value="med" />
                <label for="med"><img id="img_med" class="button" src="./ress/button_diff_normal.png"/></label>
                    
                <input type="radio" class="rad" name="difficulty" id="hard" value="hard" />
                <label for="hard"><img id="img_hard" class="button" src="./ress/button_diff_hard.png"/></label>
            </ul>

            <div id="modeSelection" class="mode-container">
                <ul id="modes" class="all-container">
                    <img id="img_points" class="button" src="./ress/button_mode_points.png"/>

                    <img id="img_temps" class="button" src="./ress/button_mode_temps.png"/>

                    <img id="img_survie" class="button" src="./ress/button_mode_survie.png"/>
                </ul>
                <ul id="points" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="5p" value="5p" />
                    <label for="5p"><img id="img_5p" class="button" src="./ress/button_points_5.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="10p" value="10p"/>
                    <label for="10p"><img id="img_10p" class="button" src="./ress/button_points_10.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="15p" value="15p" />
                    <label for="15p"><img id="img_15p" class="button" src="./ress/button_points_15.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="20p" value="20p" />
                    <label for="20p"><img id="img_20p" class="button" src="./ress/button_points_20.png"/></label>

                </ul>
                <ul id="temps" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="2m" value="2m" />
                    <label for="2m"><img id="img_2m" class="button" src="./ress/button_temps_2.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="3m" value="3m" />
                    <label for="3m"><img id="img_3m" class="button" src="./ress/button_temps_3.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="5m" value="5m" />
                    <label for="5m"><img id="img_5m" class="button" src="./ress/button_temps_5.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="10m" value="10m"/>
                    <label for="10m"><img id="img_10m" class="button" src="./ress/button_temps_10.png"/></label>

                </ul>
                <ul id="survie" class="all-container ad">
                    <input type="radio" class="rad" name="game_mode" id="15s" value="15s"/>
                    <label for="15s"><img id="img_15s" class="button" src="./ress/button_survie_15.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="30s" value="30s" />
                    <label for="30s"><img id="img_30s" class="button" src="./ress/button_survie_30.png"/></label>

                    <input type="radio" class="rad" name="game_mode" id="45s" value="45s" />
                    <label for="45s"><img id="img_45s" class="button" src="./ress/button_survie_45.png"/> </label>

                </ul>
            </div>
        
            <ul>
                <div class="slot">
                    <p class="texte">Mode pause activé <input type="radio" id="pause1" value="1" name="pause">
                    <label for="pause1" class="texte">Oui</label>
                    <input type="radio" id="pause0" value="0" name="pause">
                    <label for="pause0" class="texte">Non</label>   </p>
                           
                </div>
                <div class="slot">   
                    <p class="texte">Nombre de dots
                    <img src="ress/-" onClick="less()" id="-"/>
                    <span id="dotsNumber" class="texte"></span>
                    <img src="ress/+" onClick="more()" id="+"/>
                    <input type="hidden" name="dotsAmount" id="amountHi"/></p>
                </div>
            </ul>
        </div>
    </form>
    <?php

$database = new SQLite3("data.db");



if(isset($_POST["game_mode"]) && $_POST["game_mode"]!="" && $_POST["difficulty"]!="" && $_POST["dotsAmount"]!="" && $_POST["pause"]!="")
{
    if(stristr($_POST['game_mode'], "p") === FALSE)
    {
        $sql='select score, player from score where game_mode="'.$_POST['game_mode'].'" and difficulty="'.$_POST['difficulty'].'" and dots_amount='.$_POST["dotsAmount"].' and pause='.$_POST["pause"].' and score=(select max(score) from score where game_mode="'.$_POST['game_mode'].'" and difficulty="'.$_POST['difficulty'].'" and dots_amount='.$_POST["dotsAmount"].' and pause='.$_POST["pause"].')';
    }
    else
    {
        $sql='select score, player from score where game_mode="'.$_POST['game_mode'].'" and difficulty="'.$_POST['difficulty'].'" and dots_amount='.$_POST["dotsAmount"].' and pause='.$_POST["pause"].' and score=(select min(score) from score where game_mode="'.$_POST['game_mode'].'" and difficulty="'.$_POST['difficulty'].'" and dots_amount='.$_POST["dotsAmount"].' and pause='.$_POST["pause"].')';
    }
    $result = $database->query($sql);
    $a=$result->fetchArray();

    if($a==false)
    {
        $phrase= "Il n'y a pas encore de score pour ce mode de jeu.";
    }
    else
    {
        $phrase="Le record est ".$a[0].", tenu par ".$a["player"];
        while ($row = $result->fetchArray()) {
            $phrase=$phrase.", ".$row["player"];
    }

}

    echo "<h1 id='aya' class='texte' >".$phrase."</h1>";
    echo "<input type='hidden' id='modeSel' value='".$_POST["game_mode"]."'/>";
    echo "<input type='hidden' id='diffSel' value='".$_POST["difficulty"]."'/>";
    echo "<input type='hidden' id='dotsAmountSel' value='".$_POST["dotsAmount"]."'/>";
    echo "<input type='hidden' id='pauseSel' value='".$_POST["pause"]."'/>";
}
?>
    <div id="play" class="mode-container">
            <img id="btn_play" src="ress/button_play.png" onClick="play()"/>
            <!--<img id="btn_play_des" src="ress/button_play_desac.png"/>-->
        </div>
    </div>
    
    
    <img id="btn_back" src="ress/button_back.png" class="back" onClick="index()"/>
    
    <script src="bakery.js"></script>
    <script src="highscore_button_displayer.js"></script>
    <script src="themeSwitcher.js"></script>
    <script src="bakery.js"></script>
    <script src="themeSwitcher.js"></script>
</body>