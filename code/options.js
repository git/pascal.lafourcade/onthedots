// Get the modalOptions
var modalOptions = document.getElementById("optionsModal");

// Get the button that opens the modalOptions
var btnOptions = document.getElementById("btn_options");

// Get the <spanOptions> element that closes the modalOptions
var spanOptions = document.getElementById("close_options");

// When the user clicks the button, open the modalOptions 
btnOptions.onclick = function() {
    modalOptions.style.display = "block";
    document.getElementById("color_pickers").style.display= (displ!="points" ? "none" : "inline");
    document.getElementById("gems_picker").style.display= (displ=="points" ? "none" : "inline");
    document.getElementById(getCookie("displayMode")+"_di").classList.add(selectedTheme);
    if(displ=="both")
    {
        applyBackgrounds();
        
        backgroundBoth();
    }
}
/*
// When the user clicks on <spanOptions> (x), close the modalOptions
spanOptions.onclick = function() {
    modalOptions.style.display = "none";
}

// When the user clicks anywhere outside of the modalOptions, close it
window.onclick = function(event) {
    if (event.target == modalOptions) {
        modalOptions.style.display = "none";
        modalPatch.style.display = "none";

    }
}*/


document.getElementById("displays").addEventListener("click", function(){
    document.getElementById("color_pickers").style.display= (displ!="points" ? "none" : "inline");
    document.getElementById("gems_picker").style.display= (displ=="points" ? "none" : "inline");
    if(displ=="both")
    {
        applyBackgrounds();
        
        backgroundBoth();
    }
})

function saveOptions() {
    modalOptions.style.display = "none";
}

//initialisation des valeurs des préférences

document.getElementById("gems_di").width=size/6;
document.getElementById("points_di").style.height=size/6;
document.getElementById("points_di").style.width=size/6;
document.getElementById("both_di").style.height=size/6;
document.getElementById("both_di").style.width=size/6;
document.getElementById("both_di").style.backgroundSize=size/8+"px";
document.getElementById("btn_confirm").width=size/4;

var displ=getCookie("displayMode");

function selectDisplay(e)
{
    displ=e;
    setCookie("displayMode",e,30);
    document.getElementById("points_di").classList.remove(selectedTheme);
    document.getElementById("gems_di").classList.remove(selectedTheme);
    document.getElementById("both_di").classList.remove(selectedTheme);
    document.getElementById(e+"_di").classList.add(selectedTheme);
    backgroundBoth();
    if(displ=="both")
    {
        applyBackgrounds();
    }
    dispDots();
}

function backgroundBoth()
{
    
    for(var cpt=1;cpt<9;cpt++)
    {
        eval("slot"+cpt+".classList."+(displ=="both" ? "add" : "remove")+"('dot')");
        if(displ=="both")
        {
            eval("slot"+cpt+".style.backgroundColor='"+gemsSelected[cpt-1]+"'");
            eval("gem"+cpt+".width=size/8");
        }
        else
        {
            eval("slot"+cpt+".style.backgroundColor=''");
            eval("gem"+cpt+".width=size/6");
        }
        
    }
}

var customColorsAvailable=["#FF0000","#ffff00","#008000","#800080","#000000","#8b4513","#00ffff","#ffa500"];


for(var cpt=1;cpt<9;cpt++)
{
    eval('pickr'+cpt+` = Pickr.create({
        el: '.col`+cpt+`',
        theme: 'monolith',
        default: (getCookie("color_`+cpt+`")=="" ? customColorsAvailable[cpt-1] : getCookie("color_`+cpt+`")),
        useAsButton: true,
        
        swatches:["red","yellow","green","purple","black","brown","cyan","orange"],
        
        components: {
            
            // Main components
            preview: true,
            hue: true,
            
            // Input / output Options
            interaction: {
                hex: true,
                input: true,
                clear: true,
                save: true
            }
        }
    });`);
    
    eval("pickr"+cpt+`.on('init', function() {
        document.getElementById("col`+cpt+`").style.background=pickr`+cpt+`.getSelectedColor().toHEXA().toString();
    }).on('save', function () {
        document.getElementById("col`+cpt+`").style.background=pickr`+cpt+`.getColor().toHEXA().toString();
        colorBake();
        pickr`+cpt+`.hide();
        dispDots();
    });`);
    
}

function colorBake()
{
    for(var cpt=1;cpt<9;cpt++)
    {
        setCookie("color_"+cpt,eval("pickr"+cpt+".getColor().toHEXA().toString()"),30);
    }
}

function gemBake()
{
    for(var cpt=1;cpt<9;cpt++)
    {
        setCookie("gem"+cpt,eval("gem"+cpt).src.substring(document.getElementById("gem"+cpt).src.search("/ress")+6),30);
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    dragFrom=ev.currentTarget.parentElement;
}

var dragFrom;

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var color;
    if(dragFrom.hasChildNodes() && ev.currentTarget.hasChildNodes() && dragFrom!=ev.currentTarget)
    {
        [gemsSelected[dragFrom.id.substring(4)-1],gemsSelected[ev.currentTarget.id.substring(4)-1]]=[gemsSelected[ev.currentTarget.id.substring(4)-1],gemsSelected[dragFrom.id.substring(4)-1]];
        dragFrom.appendChild(ev.currentTarget.firstChild);
        ev.currentTarget.appendChild(dragFrom.firstChild);
        if(displ=="both")
        {/*
            var target = document.getElementById("slot"+dragFrom.id.substring(4));
            target.style.backgroundColor=target.children[0].src.substring(target.children[0].src.search("/ress")+6);

            target = document.getElementById("slot"+ev.currentTarget.id.substring(4));
            target.style.backgroundColor=target.children[0].src.substring(target.children[0].src.search("/ress")+6);*/
            applyBackgrounds();
        }
        //gemBake();
    }
}

function applyBackgrounds()
{
    for(var cpt=1;cpt<9;cpt++)
    {
        var target = document.getElementById("slot"+cpt);
        target.classList.add("dot");
        target.style.backgroundColor=target.children[0].src.substring(target.children[0].src.search("/ress")+6);
        //console.log("cookie avant : "+getCookie("gem"+cpt));
        setCookie("gem"+cpt,target.style.backgroundColor,30);
        //console.log("cookie après : "+getCookie("gem"+cpt));
        //console.log("trg : "+target.id+ "|"+target.children[0].src.substring(target.children[0].src.search("/ress")+6));
    }
    dispDots();
}

var gemsAvailable=["green","yellow","cyan","red","purple","orange","brown","black"];
var gemsSelected=[];

for(var cpt=0;cpt<8;cpt++)
{
    gemsSelected[cpt]= (getCookie("gem"+(cpt+1))=="" ? gemsAvailable[cpt] : getCookie("gem"+(cpt+1)));
    //console.log("get : "+gemsSelected[cpt]);
}

for(var cpt=1;cpt<9;cpt++)
{
    eval("var gemIcon_"+gemsSelected[cpt-1]+"=document.getElementById('gem"+cpt+"')");
    eval('gemIcon_'+gemsSelected[cpt-1]+'.src="ress/'+gemsSelected[cpt-1]+'"');
    eval('gemIcon_'+gemsSelected[cpt-1]+'.width=size/6');
    
    eval("var slot"+cpt+"=document.getElementById('slot"+cpt+"')");
    eval("slot"+cpt+".style.width=size/6");
    eval("slot"+cpt+".style.height=size/6");
    
    document.getElementById("slot"+cpt).appendChild(eval("gem"+cpt));
}

//document.getElementById("resetGems").width=size/2;
document.getElementById("resetGems2").width=size/4;

function resetGems()
{
    for(var cpt=1;cpt<9;cpt++)
    {
        setCookie("gem"+cpt,"",30);
        document.getElementById("slot"+cpt).appendChild(eval("gemIcon_"+gemsAvailable[cpt-1]));
        //console.log("sl "+"slot"+cpt+" | "+"gemIcon_"+gemsAvailable[cpt-1]);
    }
    
    if(displ=="both")
    {
        //backgroundBoth();
        applyBackgrounds();
    }
}