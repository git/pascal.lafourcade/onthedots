var light= (getCookie("light")=="" ? true : getCookie("light"));
selectedTheme = "selected"+(light=="true" ? "Day" : "Night");

document.getElementById("light").width=size/4;

if(document.getElementById("logo")!=null)
{
    document.getElementById("logo").width=size*2;
}


function switchLight() {
    light=(light=="true" ? "false" : "true");
    setCookie("light",light,30);
    applyLight(light);
}

function applyLight(li) {
    document.getElementById("light").src="ress/"+(li=="true" ? "dark" : "light")+".png";
    if(document.getElementById("logo")!=null)
    {
        document.getElementById("logo").src="ress/logo_"+(li=="true" ? "clair" : "dark")+".png";
    }

    listeModals=document.getElementsByClassName("modal-content");
    if(listeModals[0]!=null)
    {
        for(cpt=0;cpt<listeModals.length;cpt++)
        {
            listeModals[cpt].classList.add((li=="true" ? "day" : "night"));
            listeModals[cpt].classList.remove((li=="true" ? "night" : "day"));
        }
    }

    if(document.getElementById("resetGems")!=null)
    {
        document.getElementById("resetGems").src="ress/reset_"+(li=="true" ? "light" : "dark")+".png";
    }
    
    document.getElementsByTagName("body")[0].classList.add((li=="true" ? "day" : "night"));
    document.getElementsByTagName("body")[0].classList.remove((li=="true" ? "night" : "day"));

    var all = document.getElementsByClassName('selected'+(li!="true" ? "Day" : "Night"));
    var ids=[];
    for (var cpt = 0; cpt < all.length; cpt++)
    {
        ids.push(all[cpt].id);
    }

    selectedTheme = "selected"+(li=="true" ? "Day" : "Night");

    for (var cpt = 0; cpt < ids.length; cpt++) {
        var el=document.getElementById(ids[cpt]);
        el.classList.remove('selected'+(li!="true" ? "Day" : "Night"));
        el.classList.add('selected'+(li=="true" ? "Day" : "Night"));
    }
}

applyLight(light);