var modes=["points","temps","survie"];
var diffs=["easy","med","hard"];
var pauses=["0","1"];
var div=document.getElementById('buttons');
size=screen.height/6;
hideAll();
var modeSelected;
var diffSelected;
var pauseSelected;

var selectedTheme="selectedDay";

var btn_options=document.getElementById("btn_options");
btn_options.width=size/2;

var btn_patchnote=document.getElementById("btn_patchnote");
btn_patchnote.width=size;

document.getElementById("btn_play").width=size/1.25;
document.getElementById("btn_play").style.display="inline";


modes.forEach(element => {

    eval("var img_"+element+"= document.getElementById('img_"+element+"')");
    eval("img_"+element+".width=size/1.25");
    eval("img_"+element).addEventListener("mousedown",function(e)
    {
        disp(e);
        modes.forEach(element => {
            document.getElementById("img_"+element).classList.remove(selectedTheme);
        })
        document.getElementById(e.currentTarget.id).classList.add(selectedTheme);
    });

    var tab=document.getElementById(element).children;
    for(var i=1;i<tab.length;i+=2){
        tab.item(i).children[0].width=size/1.25;
        tab.item(i).children[0].addEventListener("mousedown",function(e)
        {
            var tab=document.getElementById("modeSelection").children;
            for(var cpt=1;cpt<tab.length;cpt++)
            {
                var tabChilds=document.getElementById(tab.item(cpt).id).children;
                for(var cptChild=1;cptChild<tabChilds.length;cptChild+=2)
                {
                    tabChilds.item(cptChild).children[0].classList.remove(selectedTheme);
                }
            }
            modeSelected= e.currentTarget.id.substring(4,e.currentTarget.id.length);

            document.getElementById(e.currentTarget.id).classList.add(selectedTheme);
            if(diffSelected!="" && modeSelected!="" && pauseSelected!="")
            {
                document.getElementById(modeSelected).checked=true;

                document.getElementById("high_form").submit();
            }
        });
    }
});

diffs.forEach(element => {
    eval("var img_"+element+"= document.getElementById('img_"+element+"')");
    eval("img_"+element+".width=size/1.25");
    eval("img_"+element).addEventListener("mousedown",function(e)
    {
        diffSelected= e.currentTarget.id.substring(4);
        var tab=document.getElementById("diff").children;
        for(var cpt=1;cpt<tab.length;cpt+=2)
        {
            tab.item(cpt).children[0].classList.remove(selectedTheme);
        }
        document.getElementById(e.currentTarget.id).classList.add(selectedTheme);
        if(diffSelected!="" && modeSelected!="" && pauseSelected!="")
        {
            document.getElementById(diffSelected).checked=true;
            document.getElementById("high_form").submit();
        }
    });
})

pauses.forEach(element => {
    eval("var img_pause_"+element+"= document.getElementById('img_pause_"+element+"')");
    eval("img_pause_"+element+".width=size/1.25");
    eval("img_pause_"+element).addEventListener("mousedown",function(e)
    {
        pauseSelected= e.currentTarget.id.substring(e.currentTarget.id.length-1);
        var tab=document.getElementById("diff").children;
        for(var cpt=1;cpt<tab.length;cpt+=2)
        {
            tab.item(cpt).children[0].classList.remove(selectedTheme);
        }
        document.getElementById(e.currentTarget.id).classList.add(selectedTheme);
        if(diffSelected!="" && modeSelected!="" && pauseSelected!="")
        {
            console.log("pause "+pauseSelected);
            document.getElementById("pause_"+pauseSelected).checked=true;
            document.getElementById("high_form").submit();
        }
    });
})

function disp(what, short=false)
{
    what=(short ? what : what.currentTarget.id.substring(4));
    modes.forEach(element => {
        document.getElementById(element).style.display=what==element ? "flex":"none";
    })
}

// Dots amount

var outputHi = document.getElementById("amountHi");
var amount = (getCookie("dotsAmount")=="" ? 4 : getCookie("dotsAmount"));
outputHi.value= amount;

document.getElementById("+").width=size/4;
document.getElementById("-").width=size/4;

function less()
{
  if(amount > 2)
  {
    amount--;
  }
  outputHi.value=amount;
  setCookie("dotsAmount",amount,30);
  document.getElementById("high_form").submit();
}

function more()
{
  
  if(amount<8)
  {
    amount++;
  }
  outputHi.value=amount;
  setCookie("dotsAmount",amount,30);
  document.getElementById("high_form").submit();
}

// pause oui/non

//document.getElementById("pause"+ (getCookie("pause")!="" ? getCookie("pause") : 0)).checked=true;
    

function hideAll()
{
    document.getElementById("temps").style.display="none";
    document.getElementById("points").style.display="none";
    document.getElementById("survie").style.display="none";
}

function play()
{
    var pseudo = prompt("Saisissez votre pseudo", getCookie("pseudo"));
    setCookie("pause",(document.getElementById("pause_1").checked ? "1" : "0"),30);
    if(pseudo!="")
    {
        setCookie("pseudo",pseudo,30);
    }
    
    window.location.replace("./game.html?mode="+modeSelected+"&diff="+diffSelected+"&pseudo="+ (pseudo==null ? "" : pseudo));
}

if(document.getElementById("diffSel")!=null)
{
    diffSelected=document.getElementById("diffSel").value;
    modeSelected=document.getElementById("modeSel").value;
    amount=document.getElementById("dotsAmountSel").value;
    pauseSelected=document.getElementById("pauseSel").value;
    gameSelected = (modeSelected.includes("s") ? "survie" : (modeSelected.includes("p") ? "points" : "temps"));

    disp(gameSelected, true);
    document.getElementById("img_"+diffSelected).classList.add(selectedTheme);
    document.getElementById("img_"+gameSelected).classList.add(selectedTheme);
    document.getElementById("img_"+modeSelected).classList.add(selectedTheme);
    document.getElementById("img_pause_"+pauseSelected).classList.add(selectedTheme);

    document.getElementById(diffSelected).checked=true;
    document.getElementById(modeSelected).checked=true;
    document.getElementById("pause_"+pauseSelected).checked=true;

    outputHi.value= amount;
}
else
{
    diffSelected="easy";
    modeSelected="5p";
    pauseSelected="1";
    disp("points",true);
    document.getElementById(diffSelected).checked=true;
    document.getElementById(modeSelected).checked=true;
    document.getElementById("pause_"+pauseSelected).checked=true;
    document.getElementById("img_points").classList.add(selectedTheme);
    document.getElementById("img_5p").classList.add(selectedTheme);
    document.getElementById("img_easy").classList.add(selectedTheme);
    document.getElementById("img_pause_1").classList.add(selectedTheme);
}

for(var cpt=1;cpt<9;cpt++)
    {
        document.getElementById("col"+cpt+"D").style.height=size/6;
        document.getElementById("col"+cpt+"D").style.width=size/6;
        document.getElementById("gem"+cpt+"D").style.height=size/6;
        document.getElementById("gem"+cpt+"D").style.width=size/6;
    }

function dispDots()
{
    var amountI = parseInt(amount);
    if(displ=="points")
    {
        for(var cpt=1;cpt<9;cpt++)
        {
            document.getElementById("gem"+cpt+"D").style.display="none";
            document.getElementById("col"+cpt+"D").style.display="";
        }
        for(var cpt=1;cpt<amountI+1;cpt++)
        {
            document.getElementById("col"+cpt+"D").style.visibility="visible";
            document.getElementById("col"+cpt+"D").style.backgroundColor=getCookie("color_"+cpt);
        }
        for(var cpt=amountI+1;cpt<9;cpt++)
        {
            document.getElementById("col"+cpt+"D").style.visibility="hidden";
        }
    }
    else
    {
        for(var cpt=1;cpt<9;cpt++)
        {
            document.getElementById("col"+cpt+"D").style.display="none";
            document.getElementById("gem"+cpt+"D").style.display="";

            console.log(gemsSelected[cpt-1]+"|"+cpt);

            document.getElementById("img_gem"+cpt).src="ress/"+gemsSelected[cpt-1]+".png";

            document.getElementById("gem"+cpt+"D").appendChild(document.getElementById("img_gem"+cpt));
            document.getElementById("img_gem"+cpt).style.height=size/6;
            document.getElementById("img_gem"+cpt).style.width=size/6;

            if(displ=="both")
            {
                document.getElementById("gem"+cpt+"D").classList.add("dot");
                document.getElementById("gem"+cpt+"D").style.backgroundColor=gemsSelected[cpt-1];

                document.getElementById("img_gem"+cpt).style.height=size/8;
                document.getElementById("img_gem"+cpt).style.width=size/8;

            }
            else
            {
                document.getElementById("gem"+cpt+"D").classList.remove("dot");
                document.getElementById("gem"+cpt+"D").style.backgroundColor="";
            }
        }

        for(var cpt=1;cpt<amountI+1;cpt++)
        {
            document.getElementById("gem"+cpt+"D").style.visibility="visible";
            document.getElementById("gem"+cpt+"D").src="ress/"+gemsSelected[cpt-1]+".png";
        }
        for(var cpt=amountI+1;cpt<9;cpt++)
        {
            document.getElementById("gem"+cpt+"D").style.visibility="hidden";
        }
    }
}