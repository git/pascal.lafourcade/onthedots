
//---------------
var time_in_minutes = tpsRem[1];
var current_time = Date.parse(new Date());
var deadline = new Date(current_time + time_in_minutes*60*1000);


function time_remaining(endtime){
	var t = Date.parse(endtime) - Date.parse(new Date());
	var seconds = Math.floor( (t/1000) % 60 );
	var minutes = Math.floor( (t/1000/60) % 60 );
	var hours = Math.floor( (t/(1000*60*60)) % 24 );
	var days = Math.floor( t/(1000*60*60*24) );
	return {'total':t, 'days':days, 'hours':hours, 'minutes':minutes, 'seconds':seconds};
}

var timeinterval;
function run_clock(id,endtime){
	var clock = document.getElementById(id);
	function update_clock(){
		var t = time_remaining(endtime);
		clock.innerHTML = t.hours + ":" + t.minutes + ":" + t.seconds;
		if(t.total<=0)
		{ 
			clearInterval(timeinterval);
			window.location.replace("./score.php?pts="+points+"&mode="+get['mode']+"&diff="+get['diff']+"&pseudo="+get['pseudo']);
		}
	}
	update_clock(); // run function once at first to avoid delay
	timeinterval = setInterval(update_clock,1000);
}
run_clock('timer',deadline);


var pausedC = false; // is the clock paused?
var time_left; // time left on the clock when paused

function pause_clock(){
	if(!pausedC){
    pausedC = true;
		clearInterval(timeinterval); // stop the clock
		time_left = time_remaining(deadline).total; // preserve remaining time
	}
}

function resume_clock(){
	if(pausedC){
		pausedC = false;

		// update the deadline to preserve the amount of time remaining
		console.log("tl"+time_left);
		deadline = new Date(Date.parse(new Date()) + time_left);
		console.log("dl"+deadline);

		// start the clock
		run_clock('timer',deadline);
	}
}

function addTime(sec)
{
	console.log(deadline);
	deadline.setSeconds(deadline.getSeconds() + sec);

	console.log(deadline);
}